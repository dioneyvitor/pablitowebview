import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { WebView, SafeAreaView } from 'react-native';

export default class App extends Component {
  render() {
        var INJECTEDJAVASCRIPT = `
          const meta = document.createElement('meta'); 
          meta.setAttribute('content', 
          'width=device-width, initial-scale=1.0, maximum-scale=0.85, user-scalable=0'); 
          meta.setAttribute('name', 'viewport'); 
          document.getElementsByTagName('head')[0].appendChild(meta); `
        return (
        // <WebView source={{uri: 'https://pablito-pwa-dev-h0gb5gid7m.azurewebsites.net' }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
          <WebView
              source={{ uri: 'https://localhost:5001/' }}
              scalesPageToFit={true}
              injectedJavaScript={INJECTEDJAVASCRIPT}
              scrollEnabled
          />
        </SafeAreaView>
        );
    }
}
